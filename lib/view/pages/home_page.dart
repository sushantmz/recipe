import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:recipe/provider/recipe_provider.dart';
import 'package:recipe/view/detail_page.dart';
import 'package:recipe/view/search_page.dart';
class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    final recipeData = ref.watch(recipeProvider);
    return  Scaffold(
      appBar: AppBar(
        title: Text('Recipe App'),
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: (){
                Get.to(()=> SearchPage(), transition: Transition.leftToRight);
              },
              child: Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                height: 30.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.white70,
                  border: Border.all(color: Colors.grey.withOpacity(0.5))
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.search,color: Colors.grey,),
                    Text('Search', style: TextStyle(color: Colors.grey, fontSize: 12.sp), )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
            child: Text('Find Best Recipe For Cooking', style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w600),),
          ),
          recipeData.isError ? Center(child: Text(recipeData.errorMessage),) :
          recipeData.isLoad ? Center(child: CircularProgressIndicator(),) :
          NotificationListener(
            onNotification: (ScrollEndNotification onNotification) {
              final before = onNotification.metrics.extentBefore;
              final max = onNotification.metrics.maxScrollExtent;
              // print('$before ---- $max');
              if (before == max) {
                ref.read(recipeProvider.notifier).loadMore();
              }
              return true;
            },
            child: Expanded(
              child: ListView.builder(
                  key: const PageStorageKey<String>('page'),
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.all(8.0),
                  itemCount: recipeData.recipeList.length,
                  itemBuilder: (context, index){
                    final recipes = recipeData.recipeList[index];
                    final img = recipes.content.details.images.map((e) => e.resizableImageUrl).toList().join(', ');
                    // print(img);
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: InkWell(
                        splashColor: const Color(0xff396464),
                        onTap: (){
                          Get.to(()=> DetailPage(recipe: recipes), transition: Transition.leftToRight);
                        },
                        child: Stack(
                          alignment: AlignmentDirectional.bottomCenter,
                          children: [
                            Stack(
                              alignment: AlignmentDirectional.center,
                              children: [
                                Container(
                                    width: double.infinity,
                                    height: 150.h,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.0),
                                    color: const Color(0xff396464)
                                  ),
                                  child: img.isEmpty ? Text('no image') :
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(12.0),
                                      child: Image.network(img, fit: BoxFit.fitWidth,))
                                ),
                                Text(recipes.content.details.name,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 15.sp, backgroundColor: Colors.white54),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(recipes.content.details.rating.toString(), style: TextStyle(backgroundColor: Colors.white54),),
                                  GestureDetector(
                                      onTap: (){
                                        print('heart');
                                      },
                                      child: Icon(CupertinoIcons.heart_fill, size: 30,))
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
              }),
            ),
          ),
          recipeData.isLoadMore ?
          Center(child: Transform.scale(scale: 0.6, child: CircularProgressIndicator())) : Container()
        ],
      )
      ,
    );
  }
}
