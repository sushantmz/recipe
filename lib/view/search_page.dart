import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:recipe/provider/search_provider.dart';

import 'detail_page.dart';
class SearchPage extends ConsumerStatefulWidget {
  const SearchPage({super.key});

  @override
  ConsumerState<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends ConsumerState<SearchPage> {
  TextEditingController searchController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    final searchData = ref.watch(searchProvider);
    return AnnotatedRegion(
        value: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark
        ),
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Column(
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: (){Navigator.of(context).pop();},
                      splashColor:const Color(0xff396464),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Container(
                          padding: const EdgeInsets.all(11.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6.0),
                              border: Border.all(color:const Color(0xff396464))
                            ),
                            child: Icon(Icons.arrow_back)),
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: searchController,
                        onChanged: (val){setState(() {});},
                        onFieldSubmitted: (val){
                          if(val.isEmpty){
                            print('required');
                          }else{
                            ref.read(searchProvider.notifier).getSearch(q: val.trim());
                          }
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 12.0),
                          hintText: 'Search',
                          suffixIcon: searchController.text.isEmpty ? null : InkWell(
                              splashColor: const Color(0xff396464),
                              onTap: (){setState(() {searchController.clear();});},
                              child: Icon(Icons.close)),
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(6.0)
                          )
                        ),
                      ),
                    ),
                  ],
                ),
                searchData.isLoad ?
               Center(
                   child: Transform.scale(
                   scale: 0.8,
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: CircularProgressIndicator(),
                   ))) : searchData.isError ?
               Center(child: Padding(
                 padding: const EdgeInsets.all(8.0),
                 child: Text(searchData.errorMessage),
               ),) :
               Expanded(
                    child: ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        key: const PageStorageKey<String>('page'),
                        padding: const EdgeInsets.all(8.0),
                        itemCount: searchData.recipeList.length,
                        itemBuilder: (context,index){
                      final sData = searchData.recipeList[index];
                      final img = sData.content.details.images.map((e) => e.resizableImageUrl).toList().join(', ');
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6.0),
                        child: InkWell(
                          splashColor: const Color(0xff396464),
                          onTap: (){
                            Get.to(()=> DetailPage(recipe: sData), transition: Transition.leftToRight);
                          },
                          child: Stack(
                            alignment: AlignmentDirectional.bottomCenter,
                            children: [
                              Stack(
                                alignment: AlignmentDirectional.center,
                                children: [
                                  Container(
                                      width: double.infinity,
                                      height: 150.h,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(12.0),
                                          color: const Color(0xff396464)
                                      ),
                                      child: img.isEmpty ? Text('no image') :
                                      ClipRRect(
                                          borderRadius: BorderRadius.circular(12.0),
                                          child: Image.network(img, fit: BoxFit.fitWidth,))
                                  ),
                                  Text(sData.content.details.name,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 15.sp, backgroundColor: Colors.white54),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(sData.content.details.rating.toString(), style: TextStyle(backgroundColor: Colors.white54),),
                                    GestureDetector(
                                        onTap: (){
                                          print('heart');
                                        },
                                        child: Icon(CupertinoIcons.heart_fill, size: 30,))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    })
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
