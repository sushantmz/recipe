import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:recipe/model/review_model/review.dart';
class ReviewPage extends StatelessWidget {
  final Review review;
  ReviewPage({required this.review});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reviews'),
        centerTitle: true,
        elevation: 0,
      ),
      body: review.reviews.isEmpty ? Center(child: Text('No Reviews Yet')) :  Column(
        children: [
          // Text(review.totalReviewCount.toString()),
          Expanded(
              child: ListView.builder(
                  itemCount: review.reviews.length,
                  itemBuilder: (ctx, index){
                    final reviewListData = review.reviews[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 12.0),
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: Offset(3, 3))
                            ]
                          ),
                          child: ListTile(
                            leading: reviewListData.user.pictureUrl.trim().isEmpty ?
                            CircleAvatar(
                              backgroundColor: const Color(0xff396464),
                              radius: 26,
                              child: Text('no image', textAlign: TextAlign.center, style: TextStyle(fontSize: 10.sp),),
                            ) :
                            CircleAvatar(
                                backgroundColor: const Color(0xff396464),
                              radius: 26,
                              backgroundImage: NetworkImage(reviewListData.user.pictureUrl.trim()),
                            ),
                            title: Text(reviewListData.user.displayName),
                            subtitle: Text(reviewListData.text),
                          ),
                      ),
                    );
              })
          )
        ],
      ),
    );
  }
}
