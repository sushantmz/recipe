import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:recipe/model/recipe_model/recipe.dart';
import 'package:recipe/provider/review_provider.dart';
import 'package:recipe/view/review_page.dart';
class DetailPage extends ConsumerWidget {
  final Recipe recipe;
  DetailPage({required this.recipe});

  @override
  Widget build(BuildContext context, ref) {
    final img = recipe.content.details.images.map((e) => e.resizableImageUrl).toList().join(', ');
    final reviewData = ref.watch(reviewProvider(recipe.content.details.globalId));

    final txtStyle = TextStyle(color: Colors.white);

    return Scaffold(
        body: CustomScrollView(
          key: const PageStorageKey<String>('page'),
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          slivers: [
            SliverAppBar(
              pinned: true,
              expandedHeight: 240.h,
              elevation: 0,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text('Recipe Details', style: TextStyle(fontSize: 16.sp)),
                titlePadding: const EdgeInsets.only(bottom: 10.0),
                background: img.isEmpty ? Center(child: Text('no image')) : Image.network(img, fit: BoxFit.fill),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(child: Text(recipe.content.details.name, style: TextStyle(fontSize: 14.sp),)),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Icon(Icons.watch_later_outlined,size: 18,),
                            Text(recipe.content.details.totalTime, style: TextStyle(fontSize: 11.sp),),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(Icons.star, size: 20,color: Colors.yellow,),
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 4.0),
                                child: Text('${recipe.content.details.rating.toString()}/5'),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              GestureDetector(
                                  onTap : () {
                                    Get.to(() =>ReviewPage(review: reviewData.reviewList), transition: Transition.leftToRight);
                                  },
                                  child: Text('Total Reviews: ', style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline),)),
                              reviewData.isLoad ? CupertinoActivityIndicator() : Text(reviewData.reviewList.totalReviewCount.toString()),
                            ],
                          )
                        ],
                      ),
                    ),
                    Text('Instruction : ', style: TextStyle(fontSize: 16.sp),),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: const Color(0xff396464),
                        border: Border.all(color: Colors.grey.withOpacity(0.8))
                      ),
                      child: ListView.builder(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          itemCount: recipe.content.ingredientLines.length,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (ctx, i){
                            final ing = recipe.content.ingredientLines[i];
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${i + 1}. ', style: txtStyle,),
                                     ing.wholeLine.isEmpty ? Text('n/a', style: txtStyle,) : Flexible(child: Text(ing.wholeLine,style: txtStyle)),
                                    ],
                                  ),
                                ),
                                const Divider(color: Colors.grey, thickness: 0.5,)
                              ],
                            );
                      }),
                    )
                  ],
                ),
              ),
            )
          ],
        )
    );
  }
}
