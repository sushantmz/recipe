import 'package:flutter/material.dart';
import 'package:recipe/view/pages/favourite_page.dart';
import 'package:recipe/view/pages/home_page.dart';
import 'package:water_drop_nav_bar/water_drop_nav_bar.dart';
class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {

  int selectedIndex = 0;
  late PageController pageController;
  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: pageController,
        children: <Widget>[
          HomePage(),
          FavouritePage()
        ],
      ),
      bottomNavigationBar: WaterDropNavBar(
        bottomPadding: 10.0,
        backgroundColor: const Color(0xff396464),
        inactiveIconColor: Colors.grey,
        waterDropColor: Colors.white,
        onItemSelected: (int index) {
          setState(() {
            selectedIndex = index;
          });
          pageController.animateToPage(selectedIndex,
              duration: const Duration(milliseconds: 350),
              curve: Curves.easeOutQuad);
        },
        selectedIndex: selectedIndex,
        barItems: <BarItem>[
          BarItem(
            filledIcon: Icons.home,
            outlinedIcon: Icons.home_outlined,
          ),
          BarItem(
              filledIcon: Icons.favorite_rounded,
              outlinedIcon: Icons.favorite_border_rounded),
        ],
      ),
    );
  }
}
