class Recipe {
  Content content;
  Recipe({required this.content});

  factory Recipe.fromJson(Map<String, dynamic>json){
    return Recipe(content: Content.fromJson(json['content'] ?? {}));
  }
}

class Content {
  Details details;
  List<IngredientLines> ingredientLines;
  Reviews reviews;

  Content({required this.details, required this.ingredientLines, required this.reviews});

  factory Content.fromJson(Map<String, dynamic>json){
    return Content(
        details: Details.fromJson(json['details'] ?? {}),
        ingredientLines: (json['ingredientLines'] != null) ?
        List<IngredientLines>.from(json['ingredientLines'].map((e)=> IngredientLines.fromJson(e))) : [],
        reviews: Reviews.fromJson(json['reviews'] ?? {})
    );
  }
}

//reviews
class Reviews{
  int totalReviewCount;

  Reviews({required this.totalReviewCount});

  factory Reviews.fromJson(Map<String, dynamic>json){
    return Reviews(totalReviewCount: json['totalReviewCount'] ?? 0);
  }
}

//ingredientLines
class IngredientLines{
  String ingredient;
  String wholeLine;

  IngredientLines({required this.ingredient, required this.wholeLine});

  factory IngredientLines.fromJson(Map<String, dynamic>json){
    return IngredientLines(ingredient: json['ingredient'] ?? '', wholeLine: json['wholeLine'] ?? '');
  }
}

//details
class Details {
  String directionsUrl;
  String totalTime;
  String name;
  String globalId;
  double rating;
  List<Images> images;

  Details({
    required this.directionsUrl,
    required this.totalTime,
    required this.name,
    required this.globalId,
    required this.rating,
    required this.images,
  });

  factory Details.fromJson(Map<String, dynamic> json) {
    return Details(
      directionsUrl: json['directionsUrl'] ?? '',
      totalTime: json['totalTime'] ?? '',
      name: json['name'] ?? '',
      globalId: json['globalId'] ?? '',
      rating: json['rating'] != null ? (json['rating'] as num).toDouble() : 0.0,
      images: (json['images'] != null) ? List<Images>.from(json['images'].map((e) => Images.fromJson(e))) : [],
    );
  }
}

//images
class Images {
  String resizableImageUrl;

  Images({required this.resizableImageUrl});

  factory Images.fromJson(Map<String, dynamic> json) {
    return Images(resizableImageUrl: json['resizableImageUrl'] ?? '');
  }
}
