import 'package:recipe/model/recipe_model/recipe.dart';

class RecipeState {
  final bool isLoad;
  final bool isError;
  final String errorMessage;
  final List<Recipe> recipeList;
  final int limit;
  final bool isLoadMore;

  RecipeState({
    required this.isLoad,
    required this.isError,
    required this.errorMessage,
    required this.recipeList,
    required this.limit,
    required this.isLoadMore,
  });

  RecipeState copyWith(
      {bool? isLoad,
      bool? isError,
      String? errorMessage,
      List<Recipe>? recipeList,
      int? limit,
      bool? isLoadMore}) {
    return RecipeState(
      isLoad: isLoad ?? this.isLoad,
      isError: isError ?? this.isError,
      errorMessage: errorMessage ?? this.errorMessage,
      recipeList: recipeList ?? this.recipeList,
      limit: limit ?? this.limit,
      isLoadMore: isLoadMore ?? this.isLoadMore,
    );
  }
}
