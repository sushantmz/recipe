import 'package:hive/hive.dart';

import '../recipe_model/recipe.dart';

@HiveType(typeId: 0)
class Favourite extends HiveObject{
  @HiveField(0)
  String globalId;
  @HiveField(1)
  String totalTime;
  @HiveField(2)
  String name;
  @HiveField(3)
  double rating;
  @HiveField(4)
  List<Images> images;
  @HiveField(5)
  List<IngredientLines> ingredientLines;


  Favourite({
    required this.globalId,
    required this.totalTime,
    required this.name,
    required this.rating,
    required this.images,
    required this.ingredientLines
  });


}


