class Review {
  int totalReviewCount;
  List<Reviews> reviews;

  Review({required this.totalReviewCount, required this.reviews});

  factory Review.fromJson(Map<String, dynamic> json) {
    return Review(
      totalReviewCount: json['totalReviewCount'] ?? 0,
      reviews: (json['reviews'] != null) ? List<Reviews>.from(json['reviews'].map((e) => Reviews.fromJson(e))) : [],
    );
  }
}

class Reviews {
  String createTime;
  String text;
  User user;

  Reviews({required this.createTime, required this.text, required this.user});

  factory Reviews.fromJson(Map<String, dynamic> json) {
    return Reviews(
      createTime: json['createTime'] ?? '',
      text: json['text'] ?? '',
      user: User.fromJson(json['user'] ?? {}),
    );
  }
}

//users
class User {
  String displayName;
  String pictureUrl;

  User({required this.displayName, required this.pictureUrl});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        displayName: json['displayName'] ?? '',
        pictureUrl: json['pictureUrl'] ?? '');
  }
}
