import 'package:recipe/model/review_model/review.dart';

class ReviewState {
  final bool isLoad;
  final bool isError;
  final String errorMessage;
  final Review reviewList;

  ReviewState({
    required this.isLoad,
    required this.isError,
    required this.errorMessage,
    required this.reviewList,
  });

  ReviewState copyWith({
    bool? isLoad,
    bool? isError,
    String? errorMessage,
    Review? reviewList,
  }) {
    return ReviewState(
      isLoad: isLoad ?? this.isLoad,
      isError: isError ?? this.isError,
      errorMessage: errorMessage ?? this.errorMessage,
      reviewList: reviewList ?? this.reviewList,
    );
  }
}
