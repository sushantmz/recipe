
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:recipe/api.dart';
import 'package:recipe/model/recipe_model/recipe.dart';

class SearchServices{
  static Dio dio = Dio();

  static Future<Either<String, List<Recipe>>> getSearch({required String q})async{
    try{
      final res = await dio.get(Api.searchRecipe,
      queryParameters: {
        'q': q,
        'start' : 0,
        'maxResult': 10
      },
          options: Options(
              headers:{
                'X-RapidAPI-Key' : '1bb3c27f7fmshce6748b0392b5d1p139a2ajsn862a6ae0d21c',
                'X-RapidAPI-Host' : 'yummly2.p.rapidapi.com'
              }
          )
      );

      if((res.data['feed']as List).isEmpty){
        return Left('result not found try another keyword');
      }else{
        final data = (res.data['feed']as List).map((e) => Recipe.fromJson(e)).toList();
        // print(data);
        return Right(data);
      }

    }on DioError catch (err) {
      print(err);
      return Left('$err');
    }
}
}