import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:recipe/model/review_model/review.dart';

import '../api.dart';

class ReviewServices {
  static Dio dio = Dio();

  //get reviews
  static Future<Either<String, Review>> getReview({required String globalId}) async {
    try {
      final res = await dio.get(Api.reviewList, queryParameters: {'globalId': globalId, 'limit' : 20},
          options: Options(headers: {
            'X-RapidAPI-Key': '1bb3c27f7fmshce6748b0392b5d1p139a2ajsn862a6ae0d21c',
            'X-RapidAPI-Host': 'yummly2.p.rapidapi.com'
          }));
      final data = Review.fromJson(res.data);
      // print(data.reviews);
      return Right(data);
    } on DioError catch (err) {
      print(err);
      return Left('$err');
    }
  }
}
