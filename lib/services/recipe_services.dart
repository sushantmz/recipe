
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';

import '../api.dart';
import '../model/recipe_model/recipe.dart';

class RecipeServices{
  static Dio dio = Dio();

  static Future<Either<String, List<Recipe>>> getRecipe({required int limit})async{
    try{
      final response =  await dio.get(Api.baseUrl,
        queryParameters: {
        'limit': limit
        },
        options: Options(
          headers:{
            'X-RapidAPI-Key' : '1bb3c27f7fmshce6748b0392b5d1p139a2ajsn862a6ae0d21c',
            'X-RapidAPI-Host' : 'yummly2.p.rapidapi.com'
          }
        )
      );
      final data = (response.data['feed'] as List).map((e) => Recipe.fromJson(e)).toList();
      // print(response.data['feed']);
      return Right(data);

    }on DioError catch (err) {
      print(err.message);
      return Left(err.message!);
    }

  }
}