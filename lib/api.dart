class Api{
  static const baseUrl = 'https://yummly2.p.rapidapi.com/feeds/list';
  static const reviewList = 'https://yummly2.p.rapidapi.com/reviews/list';
  static const searchRecipe = 'https://yummly2.p.rapidapi.com/feeds/search';
}