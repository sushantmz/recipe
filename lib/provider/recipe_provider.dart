
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recipe/model/recipe_model/recipe_state.dart';
import 'package:recipe/services/recipe_services.dart';

final recipeProvider = StateNotifierProvider<RecipeProvider, RecipeState>((ref) =>
    RecipeProvider(RecipeState(
      errorMessage: '',isError: false, isLoad: false, recipeList: [], limit: 8, isLoadMore: false
    ))
);

class RecipeProvider extends StateNotifier<RecipeState>{
  RecipeProvider(super.state){
    getRecipes();
  }

  Future<void> getRecipes()async{
    state = state.copyWith(isLoad: state.isLoadMore ? false : true, isError: false, );
    final res =await RecipeServices.getRecipe(limit: state.limit);
    res.fold(
            (l) => state = state.copyWith(isLoad: false, isError: true, errorMessage: l, isLoadMore: false),
            (r) => state = state.copyWith(isLoad: false, isError: false, recipeList: r , isLoadMore: false));
  }

  Future<void> loadMore()async {
    state = state.copyWith(isLoadMore: true, limit: state.limit + 8);
    getRecipes();
  }

}