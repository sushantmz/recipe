

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recipe/model/recipe_model/recipe_state.dart';
import 'package:recipe/services/search_services.dart';

final searchProvider = StateNotifierProvider.autoDispose<SearchProvider, RecipeState>((ref) =>
SearchProvider(RecipeState(errorMessage: '', isError: false, isLoad: false, isLoadMore: false, recipeList: [], limit: 0))
);

class SearchProvider extends StateNotifier<RecipeState>{
  SearchProvider(super.state);

  Future<void> getSearch({required String q})async{
    state = state.copyWith(isLoad: true, isError: false, errorMessage: '');
    final res = await SearchServices.getSearch(q: q);
    res.fold(
            (l) {
              state = state.copyWith(isLoad: false, isError: true, errorMessage: l);
            },
            (r){
              state = state.copyWith(isLoad: false, isError: false, recipeList: r);
            }
            );
  }

}