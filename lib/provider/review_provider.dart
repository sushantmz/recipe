
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recipe/model/review_model/review.dart';
import 'package:recipe/model/review_model/review_state.dart';
import 'package:recipe/services/review_services.dart';

Review reviews = Review(totalReviewCount: 0, reviews: []);


final reviewProvider = StateNotifierProvider.family<ReviewList, ReviewState, String>((ref, globalId) {
  return ReviewList(
    ReviewState(isLoad: false, isError: false, errorMessage: '', reviewList:reviews),
  )..getReviewList(globalId: globalId);
});



class ReviewList extends StateNotifier<ReviewState>{
  ReviewList(super.state);

  Future<void> getReviewList({required String globalId})async{
    state = state.copyWith(isLoad: true, isError: false, errorMessage: '');
    final res = await ReviewServices.getReview(globalId: globalId);
    res.fold(
            (l){
              state = state.copyWith(isLoad: false, isError: true, errorMessage: l);
            },
            (r){
              state = state.copyWith(isLoad: false, isError: false, reviewList: r);
            }
    );

  }
}