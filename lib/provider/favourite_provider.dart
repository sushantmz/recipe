
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:recipe/model/favourite_model/favourite.dart';
import 'package:recipe/model/recipe_model/recipe.dart';

class FavouriteProvider extends StateNotifier<List<Favourite>>{
  FavouriteProvider(super.state);

  //add to favourite

String add(Recipe recipe){
  if(state.isEmpty){
    final addToFavourite = Favourite(
      globalId : recipe.content.details.globalId,
      totalTime: recipe.content.details.totalTime,
      name: recipe.content.details.name,
      rating: recipe.content.details.rating,
      images: recipe.content.details.images,
      ingredientLines: recipe.content.ingredientLines
    );
    return 'Added to Favourite';
  }else{
    return 'Already Added to Favourite';
  }
}

}